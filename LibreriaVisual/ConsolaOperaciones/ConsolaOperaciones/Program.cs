﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibreriaOperaciones;

namespace ConsolaOperaciones
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Elige una opcion");
            Console.WriteLine("(s)uma o (r)esta");
            String opcion = System.Console.ReadLine();

            if (opcion == "s")
            {
                Console.WriteLine("SUMA");
                LibreriaOperaciones.Class1.suma(8, 2);
            }

            if (opcion == "r")
            {
                Console.WriteLine("RESTA");
                LibreriaOperaciones.Class1.resta(9, 6);
            }
        }
    }
}
