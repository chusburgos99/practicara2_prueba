package matematicas;

public class operaciones {

	public static int suma(int num1, int num2){
		int resultado = num1 + num2;
		System.out.println(num1 + "+" + num2 + "=" + resultado);
		return resultado;
	}
	
	public static int resta(int num1, int num2){
		int resultado = num1 - num2;
		System.out.println(num1 + "-" + num2 + "=" + resultado);
		return resultado;
	}
	
	public static int multi(int num1, int num2){
		int resultado = num1 * num2;
		System.out.println(num1 + "*" + num2 + "=" + resultado);
		return resultado;
	}
	
	public static int division(int num1, int num2){
		int resultado = num1 / num2;
		System.out.println(num1 + "/" + num2 + "=" + resultado);
		return resultado;
	}
}
