package main;

import java.util.Scanner;

import matematicas.operaciones;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner in = new Scanner(System.in);
		
		System.out.println("ELIGE UNA OPCION");
		System.out.println("1.suma, 2.resta, 3.multiplicacion, 4.division");
		int opcion=in.nextInt();
		
		if(opcion == 1){
			System.out.println("SUMA");
			System.out.println("Num1:");
			int num1=in.nextInt();
			System.out.println("Num2:");
			int num2=in.nextInt();
			operaciones.suma(num1, num2);
		}
		
		if(opcion == 2){
			System.out.println("RESTA");
			System.out.println("Num1:");
			int num1=in.nextInt();
			System.out.println("Num2:");
			int num2=in.nextInt();
			operaciones.resta(num1, num2);
		}
		
		if(opcion == 3){
			System.out.println("MULTIPLICACION");
			System.out.println("Num1:");
			int num1=in.nextInt();
			System.out.println("Num2:");
			int num2=in.nextInt();
			operaciones.multi(num1, num2);
		}
		
		if(opcion == 4){
			System.out.println("SUMA");
			System.out.println("Num1:");
			int num1=in.nextInt();
			System.out.println("Num2:");
			int num2=in.nextInt();
			operaciones.division(num1, num2);
		}
	}

}
