﻿using System;

namespace LibreriaOperaciones
{
    public class Class1
    {

        public static int suma(int num1, int num2)
        {
            int resultado = num1 + num2;
            Console.WriteLine(num1 + "+" + num2 + "=" + resultado);
            return resultado;
        }

        public static int resta(int num1, int num2)
        {
            int resultado = num1 - num2;
            Console.WriteLine(num1 + "-" + num2 + "=" + resultado);
            return resultado;
        }

    }
}
